# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0
package require drop_it_type 1.0
package require drop_it_flag 1.0

package provide drop_it_check 1.0

namespace eval check {

	variable CHECK

	set CHECK(check_ids) {}
}

proc check::init {} {

	variable CHECK
	
	foreach type {path name smaller larger contains} {
		lappend type_ids [type add $type]
	}
	
	set CHECK(deprioritised_type_ids) $type_ids

	set CHECK(file_type_map) {
		file             F
		directory        D
		characterSpecial C
		blockSpecial     B
		fifo             P
		link             L
		socket           S
		text             T
	}
}

# Loop through existing checks
# If matching check found, then return check_id
# Otherwise, add new check and return check id
#
# Array synompsis:
#   CHECK(idx)                     - the last serial ID used
#   CHECK(check_ids)               - list of all existing check_ids
#   CHECK(<check_id>,match)        - regular expression, glob match, or empty
#   CHECK(<check_id>,type_id)      - the type of check (see type.tcl)
#   CHECK(<check_id>,flag_ids)     - list of type specific flags (see flag.tcl)
#
proc check::add_check { type match flags } {

	variable CHECK

	set flag_ids {}
	foreach flag $flags {
		lappend flag_ids [flag add $flag -unique]
	}
	set type_id  [type add $type]

	set check_id [check::get_check_id $type_id $match $flag_ids]
	
	# If no match then add the new check
	if {$check_id == -1} {
		set check_id [check::get_next_check_id]

		set CHECK($check_id,match)    $match
		set CHECK($check_id,type_id)  $type_id
		set CHECK($check_id,flag_ids) $flag_ids

		lappend CHECK(check_ids) $check_id
	}

	return $check_id
}

proc check::get_check_id { type_id match flag_ids } {

	variable CHECK

	set search_string [join [list $type_id $match $flag_ids] {|}]
	foreach check_id $CHECK(check_ids) {
		set check_string [join [list $CHECK($check_id,type_id) $CHECK($check_id,match) $CHECK($check_id,flag_ids)] {|}]
		
		if {$search_string == $check_string} {
			return $check_id
		}
	}

	return -1
}

proc check::print_array {} {

	variable CHECK

	if {[catch {parray CHECK} msg] && ![regexp {error writing "std(out|err)": broken pipe} $msg]} {
		puts stderr $msg
	}
}

proc check::reset {} {

	variable CHECK

	array unset CHECK
	set CHECK(check_ids) {}
}

proc check::get_array {} {

	variable CHECK

	return [array get CHECK]
}

proc check::set_array { check_list } {

	variable CHECK

	array set CHECK $check_list
}

# Returns the next available check ID (serial)
proc check::get_next_check_id {} {

	variable CHECK

	incr CHECK(idx)

	if {[lsearch $CHECK(check_ids) $CHECK(idx)] > -1} {
		return [rule::get_next_check_id]
	}

	return $CHECK(idx)
}

proc check::get_flags { check_id } {

	variable CHECK
	return [flag all $CHECK($check_id,flag_ids)]
}

proc check::set_flags { check_id args } {
	foreach flag $args {
		check::set_flag $check_id $flag
	}
}

proc check::set_flag { check_id flag } {

	variable CHECK
	set flag_ids [flag add $flag -unique]

	if {[info exists CHECK($check_id,flag_ids)]} {
		set flag_ids [lsort -unique [concat $flag_ids $CHECK($check_id,flag_ids)]]
	}
	set CHECK($check_id,flag_ids) $flag_ids
}

proc check::remove_flags { check_id args } {
	foreach flag $args {
		check::remove_flag $check_id $flag
	}
}

proc check::remove_flag { check_id flag } {

	variable CHECK
	set flag_id [flag id $flag]

	if {[info exists CHECK($check_id,flag_ids)]} {
		set CHECK($check_id,flag_ids) [lremove $CHECK($check_id,flag_ids) $flag_id]
	}
}

proc check::get_type { check_id } {

	variable CHECK
	return [type get $CHECK($check_id,type_id)]
}

proc check::set_type { check_id type } {

	variable CHECK
	set type_id [type add $type]

	set CHECK($check_id,type_id) $type_id
}

proc check::get_match { check_id } {

	variable CHECK

	return $CHECK($check_id,match)
}

proc check::get_check_info { check_id } {
	lappend output [check::get_type  $check_id]
	lappend output [check::get_match $check_id]
	lappend output [check::get_flags $check_id]
	return $output
}

proc check::set_match { check_id match } {

	variable CHECK

	set CHECK($check_id,match) $match
}

proc check::check { check_id input } {

	variable CHECK

	assert {[info exists CHECK($check_id,match)]}

	set match $CHECK($check_id,match)
	set match [cfg subst $match]

	set flags [flag all $CHECK($check_id,flag_ids)]
	set type  [type get $CHECK($check_id,type_id)]

	# Look for -inverse flag
	set inverse 0
	set f_idx [lsearch $flags -inverse]
	if {$f_idx > -1} {
		set inverse 1
		set flags [lreplace $flags $f_idx $f_idx]
	}

	set check_result [check::check_$type $input $match $flags]

	if {$inverse} {
		set check_result [expr {!$check_result}]
	}
	return $check_result
}

proc check::check_name { input match {flags {}} } {

	if {[file exists $input]} {
		set input [file tail $input]
	}

	return [check::_check_match $input $match $flags]
}

proc check::check_path { input match {flags {}} } {

	if {[file exists $input]} {
		set input [file_utils::get_absolute_path $input]
	}

	return [check::_check_match $input $match $flags]
}

proc check::_check_match { input match flags } {
	set match_method   [check::get_match_method  $flags]
	set case_sensitive [check::is_case_sensitive $flags]

	# Check if file path matches
	switch -exact $case_sensitive|$match_method {
	{0|-regexp} { set result [regexp -nocase -- $match $input] }
	{1|-regexp} { set result [regexp -- $match $input] }
	{0|-glob}   { set result [string match -nocase $match $input] }
	{1|-glob}   { set result [string match $match $input] }
	default     { set result 0 }
	}

	return $result
}

proc check::get_match_method { flags } {

	set match_method [cfg get DEFAULT_MATCH_METHOD -regexp]
	if {[lsearch $flags {-glob}] > -1} {
		set match_method -glob
	}
	if {[lsearch -regexp $flags {^-re?(gexp?)?$}] > -1} {
		set match_method -regexp
	}

	return $match_method
}

proc check::is_case_sensitive { flags } {
	set case_sensitive [cfg get DEFAULT_CASE_SENSITIVE 1]
	if {[lsearch $flags {-nocase}] > -1} {
		set case_sensitive 0
	}
	if {[lsearch $flags {-case}] > -1} {
		set case_sensitive 1
	}
	return $case_sensitive
}

proc check::check_is_empty { input {match {}} {flags {}} } {
	return [file_utils::is_empty $input]
}

proc check::check_contains { input match {flags {}} } {

	set file_type [file_utils::file_type $input]
	set rule_matched 0

	switch -exact $file_type {
	directory { 
		if { ![catch { set subfiles [glob -types {f d r} -path $input/ *] } msg] } {
			foreach subfile $subfiles {
				set rule_matched [check::check_name $subfile $match $flags]
				if {$rule_matched} {
					break
				}
			}
		} else {
			log INFO {Directory $input doesn't contain any files or directories: $msg}
		}
	}
	file {
		touch contains_exec
		
		# Get the file extension preferring the presense of a double extension if
		# it exists. For example prefer the .tar.gz extension over .gz
		if {[regexp {\.((?:[0-9A-Za-z]+\.)?([0-9A-Za-z]+))$} $input --> file_ext1 file_ext2]} {
		
			# Retrieve the contains executable if configured for the found extension
			foreach ext [list $file_ext1 $file_ext2] {
				if {$ext == {}} {
					continue
				}
				set ext [string toupper $ext]
				set ext [string map {. _} $ext]
				set contains_exec [cfg get CONTAINS_EXEC_$ext {}]
				if {$contains_exec != {}} {
					break
				}
			}
		}

		# If no configuration exists for the given extension then we need to use
		# the default.
		if {$contains_exec == {}} {
			set contains_exec [cfg get CONTAINS_EXEC_DEFAULT {}]

			# It is possible that no default contains executable has been defined
			# in which case we cannot confirm that the file actually contains the
			# expression we are searching for.
			if {$contains_exec == {}} {
				log WARN {No contains command found for extension $ext}
				return 0
			}
		}

		set replace_map [list\
			%file%     [string map {{ } {\ }} $input]\
			%contains% $match\
		]

		set contains_exec [string map $replace_map $contains_exec]

		log DEBUG {Executing contains executable: $contains_exec}

		if {[catch {set output [exec {*}$contains_exec]} msg]} {
			# child process exited abnormally is usually just the exit
			# signal indicating that there is not a match.
			if {$msg != {child process exited abnormally}} {
				log ERROR {check::check_contains - Error occurred while executing $contains_exec due to $msg}
			} else {
				log DEBUG {check::check_contains - No match found while executing $contains_exec}
			}
		} elseif {$output != {}} {
			log DEBUG {check::check_contains - match found: $output}
			set rule_matched 1
		}
	}
	text {
		set rule_matched [check::check_match $input $match $flags]
	}
	default {
		log ERROR {check::check_contains - Unknown file type ($file_type)}
	}
	}
	return $rule_matched
}

proc check::check_exists { input match {flags {}} } {
	return [file exists $match]
}

proc check::check_writable { input match {flags {}} } {
	return [file writable $input]
}

proc check::check_readable { input match {flags {}} } {
	return [file readable $input]
}

proc check::check_executable { input match {flags {}} } {
	return [file executable $input]
}

proc check::check_not_exists { input match {flags {}} } {
	return [expr {![file exists $match]}]
}

proc check::check_windows { {input {}} {match {}} {flags {}} } {
	return [expr {$::tcl_platform(platform) == {windows}}]
}

proc check::check_unix { {input {}} {match {}} {flags {}} } {
	return [expr {$::tcl_platform(platform) == {unix}}]
}

proc check::check_mac { {input {}} {match {}} {flags {}} } {
	return [expr {$::tcl_platform(platform) == {macintosh}}]
}

proc check::check_java { {input {}} {match {}} {flags {}} } {
	return [expr {$::tcl_platform(platform) == {java}}]; # OS/400
}

proc check::check_type { input match {flags {}} } {

	variable CHECK

	set file_type [file_utils::file_type $input]
	set type [string map $CHECK(file_type_map) $file_type]
	return [string match *$type* $match]
}

proc check::check_newer { input match {flags {}} } {

	if {![file exists $input]} {
		return 0
	}
	if {![string match {-*} $match]} {
		prepend match -
	}
	return [expr {[file mtime $input] > [clock add [clock seconds] {*}$match]}]
}

proc check::check_older { input match {flags {}} } {

	if {![file exists $input]} {
		return 0
	}
	if {![string match {-*} $match]} {
		prepend match -
	}
	return [expr {[file mtime $input] < [clock add [clock seconds] {*}$match]}]
}

proc check::check_smaller { input match {flags {}} } {
	set size [file_utils::file_size $input]
	set comp [file_utils::decode_human_readable_size $match]
	return [expr {$size < $comp}]
}

proc check::check_larger { input match {flags {}} } {
	set size [file_utils::file_size $input]
	set comp [file_utils::decode_human_readable_size $match]
	return [expr {$size > $comp}]
}

# Sort the check_ids based on type to get optimal performance
proc check::get_optimal_check_order { check_ids } {
	
	variable CHECK

	touch check_order
	touch deprioritised

	foreach check_id $check_ids {
		if {[lsearch $CHECK(deprioritised_type_ids) $CHECK($check_id,type_id)] > -1} {
			lappend deprioritised $check_id
			continue
		}
		lappend check_order $check_id
	}
		
	lappend check_order {*}$deprioritised
	
	return $check_order
}


proc check::to_string { check_id } {

	variable ::rule::PARAMS

	touch check match

	lassign [check::get_check_info $check_id] check_type check_match check_flags

	foreach check_flag $check_flags {
		if {[lindex [nvl ::rule::PARAMS($check_flag) {}] 3] == {check_flag}} {
			lappend check $check_flag
		}
	}

	switch -- $check_type {
	name -
	path {
		set param_pattern [expr {[lsearch -exact $check_flags {-re}] > -1 ? {-re} : {-glob}}]
		lappend check -$check_type
	}
	default {
		set param_pattern *-$check_type
	}
	}
	set param_name [array names ::rule::PARAMS $param_pattern]

	if {[llength $param_name] != 1} {
		log ERROR {check::to_string: Unexpected error, parameter for check_id $check_id is {$param_name}}
		log ERROR {check::to_string: Check info is {$check_type} {$check_match} {$check_flags}}
		return
	}

	lassign $::rule::PARAMS($param_name) - p_num_args - p_param_type -

	set param_values [expr {$param_name == {-type} ? [split $check_match {}] : [list $check_match]}]
	foreach param_value $param_values {
		# If this is not one of the types that can have multiple parameters then
		# use the parameter name as the check itself.
		if {[lsearch {type platform} $p_param_type] == -1} {
			if {$p_num_args == 0} {
				lappend check $param_name
			} else {
				lappend check $param_name \{$param_value\}
			}
			continue
		}
		# Apply a reverse lookup by looping through all possible parameters and for
		# each parameter that has the same parameter type check if the default value
		# matches or not. If it does then we have identified the check we are using.
		foreach p $::rule::PARAMS(parameters) {
			if {[lindex $::rule::PARAMS($p) 3] == $p_param_type} {
				if {[lindex $::rule::PARAMS($p) 2] == $param_value} {
					lappend check $p
					break
				}
			}
		}
	}

	return [list $check_type $check $match]
}

proc check::explain { check_id input {matches 1} } {

	lassign [check::to_string $check_id] check_type checks match 

	set show_matches 0
	set show_does 0
	set show_is 0

	set checks [lremove $checks -$check_type]
	switch -- $check_type {
	is_empty {
		set check_type [file_utils::file_type $input]
		set match {empty}
		set show_is 1
	}
	contains {
		set check_type [file_utils::file_type $input]
		set checks [linsert $checks 0 {contain}]
		set show_does 1
	}
	exists {
		set check_type {}
		set match {exist}
		set show_does 1
	}
	not_exists {
		set check_type {}
		set match {not exist}
		set show_does 1
	}
	default {
		set show_matches 1
	}
	}

	touch check_output
	lappend check_output [lindex {- +} $matches]
	lappend check_output $check_type
	if {[cfg enabled VERBOSE]} {
		lappend check_output "check ($check_id)"
	}
	if {$show_matches == 1} {
		lappend check_output [lindex {{does not match} {matches}} $matches]
	}
	if {$show_does == 1} {
		lappend check_output [lindex {{does not} does} $matches]
	}
	if {$show_is == 1} {
		lappend check_output [lindex {{is not} is} $matches]
	}
	lappend check_output $checks
	lappend check_output $match
	set check_output [lremove $check_output {}]
	return [join $check_output { }]
}
