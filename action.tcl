# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0
package require exec_utils 1.0
package require drop_it_destination 1.0
package require md5

package provide drop_it_action 1.0

namespace eval action {

	set pname [opt::proc ::action --preset list]
	opt::option $pname add            --proc [list action::add]
	opt::option $pname get            --proc [list action::get_action]
	opt::option $pname destination    --proc [list action::get_destination]
	opt::option $pname destination_id --proc [list action::get_destination_id]
	opt::option $pname flags          --proc [list action::get_flag]
}

# Loop through existing actions and return the associated action_id if found,
# otherwise add the new action and return the action_id.
#
# Match actions:
#   --exec           - executes a given script / command
#   --date           - prefixes files with the file timestamp
#   --rename         - renames files based on predefined regular expressions
#   --move           - moves files to given directory
#   --stop           - stops processing of the given file (no further rules checked)
#   --recurse        - recurses a directory, re-processing files within
#   --delete         - deletes a file (or moves to trash)
#
proc action::add { action destination flags } {

	set destination_id [destination add $destination]
	set entry [list $action $destination_id $flags]

	set action_id [lsearch -exact $::ACTION $entry]
	
	# If no match then add the new action
	if {$action_id == -1} {
		set action_id [llength $::ACTION]
		lappend ::ACTION $entry
	}

	return $action_id
}

proc action::get_action { action_id } {

	return [lindex [lindex $::ACTION $action_id] 0]
}

proc action::get_destination_id { action_id } {

	return [lindex [lindex $::ACTION $action_id] 1]
}

proc action::get_destination { action_id } {

	return [destination get [action::get_destination_id $action_id]]
}

proc action::get_flag { action_id } {

	return [lindex [lindex $::ACTION $action_id] 2]
}

#
# Moves a file to a given destination.
#
# If the destination directory does not already exists the script
# will attempt to create it automatically.
#
# This assumes that the destination is a directory.
#
proc action::move_action { file destination flags } {

	file_utils::ensure_exists $destination
	set dest [file join $destination [file tail $file]]

	if {[lsearch $flags {-suffix}] != -1} {
		append dest [drop_it::generate_dropit_suffix $file]
	}

	return [move $file $dest $flags]
}

#
# This proc does the actual moving of files.
#
# It is used by other procedures like move_action, date_action and rename_action.
#
proc action::move { file dest flags } {
	
	if {[file exists $dest]} {
		file stat $file FILE
		file stat $dest DEST

		if {[lsearch $flags {-suffix}] != -1} {
			log WARN { - $file exists at destination ($dest) deleting instead}
			return [delete_action $file {} {}]
		} elseif {$FILE(dev) == $DEST(dev) && $FILE(ino) == $DEST(ino)} {
			log WARN { - Not moving file $file to same location}
			return [list NOT_OK $file]
		} elseif {$FILE(mtime) > $DEST(mtime)} {
			log WARN {Older file already exists at destination ($dest), replacing}
			set output [move_action $dest [cfg get DEF_TRASH] -suffix]
			log DEBUG {Replace output was: $output}
		} elseif {$FILE(size) == $DEST(size)} {
			log WARN { - Duplicate exists at destination ($dest), moving to trash instead}
			return [move_action $file [cfg get DEF_TRASH] -suffix]
		} else {
			log WARN { - Newer file already exists at destination ($dest), moving to trash instead}
			return [move_action $file [cfg get DEF_TRASH] -suffix]
		}
	}

	if {[cfg enabled DRY_RUN]} {
		set dest_dir [file dirname $dest]
		log DEBUG { - Pretended to move $file to $dest_dir (dry run)}
	} elseif {[catch {file rename $file $dest} msg]} {
		log ERROR { - Failed to rename $file to $dest due to $msg}
		return [list NOT_OK $dest]
	} else {
		set dest_dir [file dirname $dest]
		log INFO { - Moved $file to $dest_dir}
	}
	
	return [list OK $dest]
}

#
# Physically removes a given file unless a destination is provided
# in which case the delete is regarded as a "move to trash / recycle bin".
#
proc action::delete_action { file destination flags } {

	if {![file exists $file]} {
		log INFO { - Unable to delete non-existing file $file}
		return [list NOT_OK $file]
	}

	if {[lsearch $flags --if_empty] != -1 && ![file_utils::is_empty $file]} {
		log DEBUG { - Not deleting $file as it is not empty}
		return [list NOT_OK $file]
	}

	# If we are deleting and we have a destination, presume that we are moving to trash
	# rather than explicitly deleting the file.
	if {$destination != {}} {
		return [move_action $file $destination {}]
	}

	set outcome [file_utils::delete_file $file]
	if {$outcome != 1} {
		log WARN { - File $file not deleted}
	}
	return [list [string map {1 OK 0 NOT_OK} $outcome] {}]
}

#
# Attempts to execute whatever is in the destination variable.
#
proc action::exec_action { file destination flags } {

	if {[catch {
		set output [exec_utils::execute $destination]
		set code OK
	} msg]} {
		set code NOT_OK
	}
	# Currently we do not let the executable change the input file, so
	# returning {} here. See rule::action for how the return value is
	# used. It might be possible to allow the exec action to override
	# the input by introducing a special flag, but it would depend on
	# that the output of the executable is the new file name and the
	# new file name alone.
	return [list $code {}]
}

#
# Retrieves all readable files and directories within a given directory and processes them.
#
proc action::recurse_action { directory destination flags } {

	touch types files

	# Glob Types:
	#    b (block special file)
	#    c (character special file)
	#    d (directory)
	#    f (plain file)
	#    l (symbolic link)
	#    p (named pipe)
	#    s (socket)
	#
	#    r (readable)
	#    w (writable)
	#    x (special permissions)
	#
	regexp -- {-types [\"\{\(\'\[]([bcdflpsfdrwx ]+)[\"\}\)\'\]]} $flags match types]
	if {$types == ""} {
		set types {f d r}
	}
	# List all readable files and directories.
	# Links are not included.
	if {[catch {
		set files [glob -types $types -path [file normalize $directory]/ *]
	} msg]} {
		log WARN {No files found while recursing directory $directory}
	}

	if {$files != {}} {
		log INFO {Processing files and directories within $directory}
		log INFO { - $files}

		drop_it::process_file_args $files
	}

	if {[cfg enabled DELETE_DIRECTORY_IF_EMPTY_AFTER_RECURSE 1]} {
		if {[file_utils::is_empty $directory]} {
			log INFO {Deleting empty directory $directory after recurse}
			file delete $directory
		}
	}

	return [list OK]
}

#
# Proc to rename file names making them follow a certain standard.
#
# The list needs to be of the following format:
#    {-nocase?} {match} {replace with}
#
proc action::rename_action { file destination flags } {

	set file_name [file tail $file]
	set file_ext  [string tolower [file extension $file]]
	
	if {[lsearch $flags {-capitali*e}] != -1} {
		set output [text_utils::string_capitalize $file_name]
	} else {
		set output $file_name
	}

	if {![string is list $destination]} {
		log WARN {Rename replace list is not a list ($destination), ignoring action for file $file}
		return [list NOT_OK $file]
	}

	set case_sensitive [cfg enabled CASE_SENSITIVE_RENAME_ACTION 0]
	foreach {match with} $destination {
		if {$case_sensitive} {
			regsub -all -- $match $output $with output
		} else {
			regsub -nocase -all -- $match $output $with output
		}
	}
	
	# Changing file extension to be lower case
	set output [string tolower $output [string last . $output] end]

	set dest [file join [file dirname $file] $output]
	
	# If no rename necessary
	if {[regsub -all {\\|/} $file {}] == [regsub -all {\\|/} $dest {}]} {
		log INFO { - File $file_name is clean, no rename necessary}
		set ret [list OK $file]
	} else {
		log INFO { - Renaming file:\n   <-- $file_name\n   --> $output}
		
		if {[file exists $dest]} {
			set backup_suffix [drop_it::generate_dropit_suffix]
			set new_dest $dest$backup_suffix
			move $file $new_dest $flags
			set ret [move $new_dest $dest $flags]
		} else {
			set ret [move $file $dest $flags]
		}
	}
	
	return $ret
}

#
# Pre-pends a file name with the file's modification time.
#
proc action::date_action { file destination flags } {

	set file_name [file tail $file]

	if {$destination == {}} {
		set destination [file dirname $file]
	}

	# Do not add date if it already exists
	set date_format [cfg get FILE_DATE_PREFIX_FORMAT {%Y.%m.%d.}]
	if {![regexp -- [text_utils::convert_clock_format_to_regexp $date_format] $file_name]} {
		set time [file mtime $file]
		set date_prefix [clock format $time -format $date_format]
		prepend file_name ${date_prefix}
	}

	set dest [file join $destination $file_name]

	return [move $file $dest $flags]
}

#
# Proc to stop further search and processing.
#
proc action::stop_action { file destination flags } {

	log INFO { - Stopping due to match on file $file}

	return [list OK $file]
}

