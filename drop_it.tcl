#!/bin/sh
# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.
# APPTAG|tcl|drop-it|Moves, renames, procses or deletes files and folders based on predefined rules
# If running in a UNIX shell, restart underes tclsh on the next line \
exec tclsh "$0" ${1+"$@"}

for {set script [file normalize [info script]]} {[file type $script] == {link}} {set script [file readlink $script]} {}
lappend ::auto_path [file dirname $script] [file join [file dirname [file dirname $script]] tapp]

package require drop_it 1.0

#
# Drop-It configuration options.
#
# Most of these can be enabled from the command line by passing in flags to
# the drop-it script/application. The rest may only be of interest if advanced
# features are required.
#
# In general no configuration changes other than rules setup and perhaps log
# directory are needed for normal usage.
#
# @cfg DRY_RUN                                   Do a dry-run, i.e. pretend to
#                                                do the job, but don't change
#                                                or move files around. This is
#                                                enabled by passing ‑d /
#                                                ‑‑dry‑run to drop-it.
# @cfg RULES                                     This configuration item
#                                                defines all the drop-it rules.
# @cfg ACTION_RULE_MATCH                         Determines whether to enable
#                                                rule actions. Defaults to
#                                                being enabled, but can be
#                                                disabled by passing
#                                                ‑‑no_action to drop-it. This
#                                                may be considered as an
#                                                alternative to a dry‑run and
#                                                can be useful in a few special
#                                                cases, but in general the
#                                                recommendation would be to do
#                                                a dry‑run instead.
# @cfg CASE_SENSITIVE_RENAME_ACTION              This option enables case
#                                                sensitive rename operations
#                                                for files being mangled with
#                                                with the ‑‑rename action.
# @cfg CFG_FILE                                  This defines the name and
#                                                location of the rules
#                                                configuration file. Defaults
#                                                to "drop_it_rules.cfg" in
#                                                the drop-it script directory.
# @cfg CHECKSUM_DROPIT_SUFFIX                    When moving files to the
#                                                "trash" directory the file is
#                                                appended with a drop-it suffix
#                                                to 1) indicate that it has
#                                                been moved there by drop-it
#                                                and 2) avoid name clashes and
#                                                overwriting files. If this
#                                                config is enabled then a
#                                                checksum (MD5 or otherwise) is
#                                                used as the suffix. The
#                                                benefit of this is that it to
#                                                some degree prevents duplicate
#                                                files taking up space in the
#                                                "trash" directory. Defaults to
#                                                1 (enabled).
# @cfg CHECKSUM_CMD                              This defines the command to
#                                                execute in order to calculate
#                                                the file checksum. Defaults
#                                                to: md5sum "%file%". The
#                                                %file% placeholder is
#                                                substituted with the path to
#                                                the file when the command is
#                                                being executed.
# @cfg CONTAINS_EXEC_DEFAULT                     This defines the executables
#                                                to execute in relation to the
#                                                ‑contains check. There are two
#                                                placeholders: %file% which
#                                                will be replaced by the file
#                                                being processed and %contains%
#                                                which will be replaced by the
#                                                ‑contains expression for the
#                                                rule. This configuration item
#                                                defines the default command to
#                                                execute if one has not been
#                                                defined for the relevant file
#                                                type (this will typically be a
#                                                standard grep). This is made
#                                                configurable for
#                                                cross-platform purposes and to
#                                                allow customised searches
#                                                through files. Example
#                                                configuration:
#                                                grep -Ei "%contains%" "%file%"
# @cfg CONTAINS_EXEC_<file_extension>            Custom command for file type
#                                                specific contains checks. The
#                                                extension is in uppercase and
#                                                dots are replaced by
#                                                underscores. For example we
#                                                could match on a .tar.gz file
#                                                based on zgrep by defining
#                                                CONTAINS_EXEC_TAR_GZ = zgrep
#                                                -Ei "%contains% %file%, or
#                                                we could check if the archive
#                                                contains a file name matching
#                                                the contains expression with:
#                                                tar -ztf %file%  | grep -Ei
#                                                "%contains%". Other
#                                                alternatives could be to use
#                                                pdftotext to search through
#                                                the content of PDF files, to
#                                                use unoconv to search through
#                                                .doc and many other document
#                                                formats, or exiftool to match
#                                                on image EXIF data.
# @cfg CUSTOM_RULE_PARAM_OVERRIDE                Experimental functionality
#                                                allowing override of rule
#                                                definitions. This is primarily
#                                                intended for plugins
#                                                / extensions.
# @cfg DEFAULT_CASE_SENSITIVE                    This indicates whether rule
#                                                checks are by default case
#                                                sensitive unless specifically
#                                                indicated. Defaults to enabled.
# @cfg DEFAULT_RULE_STATUS                       This determines the default
#                                                status of rules unless
#                                                explicitly defined in the
#                                                rules configuration setup.
#                                                This defaults to "A" (active)
#                                                and individual rules can be
#                                                suspended (status "S") by
#                                                using the ‑‑suspended flag
#                                                in the drop-it rules
#                                                configuration.
# @cfg DEF_TRASH                                 The default "trash" directory
#                                                to move "deleted" files to.
# @cfg DELETE_DIRECTORY_IF_EMPTY_AFTER_RECURSE   When using the ‑‑recurse
#                                                action to process files, this
#                                                config item indicates whether
#                                                to attempt to remove the
#                                                directory if it is empty after
#                                                processing all files. Defaults
#                                                to enabled. This is more for
#                                                convenience as it may be
#                                                annoying with left-over empty
#                                                directories.
# @cfg EXPLAIN_RULE_MATCH                        This is enabled by passing ‑e
#                                                / ‑‑explain to drop-it.
#                                                Enabling this in config is the
#                                                same as always passing the
#                                                ‑‑explain option to drop-it
#                                                which enables additional
#                                                output to explain why a file
#                                                did or did not match a given
#                                                rule.
# @cfg EXPLAIN_ALL_RULES                         This is enabled by passing ‑a
#                                                / ‑‑explain_all to drop-it.
#                                                By enabling this additional
#                                                explain output will be printed
#                                                for all rules whether they are
#                                                relevant to the given file or
#                                                not.
# @cfg EXPLAIN_CHECK_INDENT                      Look and feel option which
#                                                determines the indent used for
#                                                the explain output, defaults
#                                                to 3.
# @cfg FILE_DATE_PREFIX_FORMAT                   If a rule specifies the ‑‑date
#                                                action then the matching file
#                                                will be prefixed with a date.
#                                                This configuration item
#                                                specifies the date format to
#                                                use and it defaults to
#                                                "%Y.%m.%d." (clock format).
# @cfg LOG_RULES                                 If enabled then all rules in
#                                                use will be logged when files
#                                                are being processed. This
#                                                might be useful if rules are
#                                                changing often or an accurate
#                                                record of what rules were in
#                                                play when a certain match was
#                                                made is needed. Enable via
#                                                config, defaults to disabled.
#                                                ‑‑rules parameter to drop-it.
# @cfg NO_REPORT_ACTIONS                         This lists actions which
#                                                should not be reported as
#                                                a match (i.e. process
#                                                silently) unless verbose
#                                                output or debug log level is
#                                                enabled. This defaults to the
#                                                "stop" action.
# @cfg NOTIFY                                    This enables desktop
#                                                notification of the drop-it
#                                                report. This is enabled with
#                                                the ‑‑notify parameter and is
#                                                currently limited to Linux
#                                                operating systems.
# @cfg PRINT_ARGUMENTS                           This enables printing of all
#                                                the parameters that are passed
#                                                to drop-it. This can be useful
#                                                if drop-it is being called
#                                                by some background task such
#                                                as a cronjob for example.
#                                                This can also be enabled by
#                                                passing in the ‑‑list_args
#                                                parameter to drop-it.
# @cfg PRINT_CONFIGURATION                       Enables listing of all
#                                                configuration items on
#                                                startup. This can be enabled
#                                                by passing in the ‑‑list_conf
#                                                parameter to drop-it.
# @cfg PRINT_RULES                               Enables listing of all rules
#                                                on startup. This can be
#                                                enabled by passing in the
#                                                ‑‑rules parameter to drop-it.
# @cfg TIME                                      Enables timing of rules for
#                                                the current run. When enabled
#                                                the list of rules will be
#                                                printed along with information
#                                                about how long time it took to
#                                                check each rule against the
#                                                given file(s).
# @cfg FUNC_STATS                                Enables tracking of long time
#                                                rule statistics. This includes
#                                                rules performance (number of
#                                                checks performed, number of
#                                                matches and average time spent
#                                                performing matches). If this
#                                                is enabled then additional
#                                                information is added to the
#                                                ‑‑rules option output which
#                                                indicates which rules are
#                                                being used and which can be
#                                                removed.
# @cfg STAT_RUN_IN_DAYS                          Defines the period (number of
#                                                days) we are interested in
#                                                with regards to evaluating how
#                                                relevant the drop-it rules are
#                                                in the long run. 
#                                                Defaults to 90 days.
#                                                In practice this means that it
#                                                will take 90 days to gather
#                                                statistics and a further 90
#                                                days will pass before negative
#                                                rules are starting to display.
#                                                Afterwards we should have a
#                                                relatively good impression of
#                                                what rules are actually being
#                                                used and which ones can be
#                                                removed. When listing rules
#                                                one or more stars ( * )
#                                                indicates that the rule has
#                                                matched a file within 72 days
#                                                (40% of 180). One or more
#                                                hyphens ( - ) means that no
#                                                files have matched the given
#                                                rule within 108 days (lower
#                                                40% of 180). If no stars or
#                                                hyphens are displayed then
#                                                that means that the last time
#                                                the rule matched a file was
#                                                in the middle 20% (i.e.
#                                                between 72 and 108 days ago).
#                                                Note that changing this config
#                                                only affects the * column when
#                                                listing rules, so is perfectly
#                                                safe to play around with.
# @cfg STATS_EXTENSION                           If FUNC_STATS is enabled then
#                                                a stats file will be saved
#                                                with the same name, but
#                                                different file extension, as
#                                                the rules configuration file.
#                                                This config determines the
#                                                extension to use. Defaults to
#                                                ".stat".
# @cfg PARSE_RULES_ON_CHANGE_ONLY                If drop-it is set up with a
#                                                significant amount of rules
#                                                then only parsing the rules
#                                                when the rules file has
#                                                changed may have performance
#                                                benefits. If this is enabled
#                                                then the parsed rules are
#                                                dumped to a .db file that are
#                                                loaded on the next run. The
#                                                .db file will be replaced
#                                                whenever the timestamp on the
#                                                rules configuration file
#                                                changes. The performance
#                                                benefit of enabling this is
#                                                unlikely to be noticeable with
#                                                less than 300 rules. Also note
#                                                that if the rules have OS
#                                                specific variance then
#                                                enabling this may have
#                                                unforeseen consequences.
# @cfg HELP_EXAMPLES                             Config item for internal use.
#                                                Contains the information given
#                                                when the ‑‑examples flag is
#                                                passed in.
# @cfg LIMIT_TO_RULES                            Config item for internal use.
#                                                Specifies which of the rules
#                                                in the rules configuration
#                                                file that should be used. This
#                                                config item is set when the
#                                                ‑‑rule parameter is used.
# @see tapp_log for logging configuration options
#
namespace eval drop_it {}


cfg file [file rootname [this]].cfg
cfg set HELP \
{Usage: drop-it [OPTION]... [RULES_CONFIG] [FILE]...

Moves, renames, processes or deletes files and folders based on predefined rules.

Optional arguments:
%args%
(arguments are not dependent on which order they come in)

This depends on environment configuration set up (logging etc.), see top of drop_it.tcl for available options.

Warning! Please handle with care. This tool is not dishwasher safe.
}

cfg lappend ARGS\
	{-c --config}           {specifies the rule configuration file to use}\
	{-d --dry-run}          {pretend to do the job, but don't process or move files around}\
	{-h --help}             {display this help}
	
# Escape notes for the HELP_EXAMPLES:
# drop-it --rules | sed -e "s/\\\\/\\\\\\\\\\\\\\\\/g" | sed -e "s/{/\\\\{/g" | sed -e "s/}/\\\\}/g" | sed -e "s/\[/\\\\[/g" | sed -e "s/\]/\\\\]/g"

cfg set HELP_EXAMPLES {
=== Setting up drop-it rules ===

The drop-it rules configuration file drop_it_rules.cfg will be automatically created the first time the script is run.

Note that the the regular expressions that we add should be wrapped in curly braces (this is how strings are represented in Tcl).

Example rules:

1) If the file is named *.exe, then don't check this file against any further rules (stop processing the file).
   --stop -file -glob \{*.exe\}\\\\

2) If this is a file inside the "My Documents" folder, then don't process this file.
   --stop -path -re \{\[\\\\\\\\/\]My Documents\[\\\\\\\\/\]\}\\\\

3) If the file is a document or a text file and the name includes "cover letter" then move that to our designated folder.
   --move ~/Documents/CV/cover_letters/ -nocase -file -re \{cover.?letter.*\.(docx?|txt)$\}\\\\

4) If the file extension is .bak then prefix the file name with a date and move the file to our backup directory.
   --date --move ~/Backup/ -nocase -glob \{*.bak\}\\\\

5) If the file is a temporary file that ends with ~ then just delete it.
   --delete -glob \{*~\}\\\\

6) If the file is named Anti-Vir.Dat then let's "delete" (read move) it to our trash directory (defined with the configuration item DEF_TRASH).
   --delete %DEF_TRASH% -file -glob \{Anti-Vir.Dat\}\\\\

7) If this is a directory named "Downloads", then process all files inside this directory.
   --recurse -dir -glob \{Downloads\}\\\\

8) If this is a .doc file then move it to the "My Documents" folder, but only if we are running this under windows.
   --windows --move "$::env(HOME)/My Documents" -nocase -re \{\[.\]docx?$\}\\\\

9) We had a rule that didn't do what we thought it would do so we disabled it by adding -- or --suspended.
   -- --delete -file -glob \{*.*\}\\\\

10) If this is a .txt file that contains the text "Dear Margaret" then let's chuck that into the bin.
   --delete %DEF_TRASH% -file -glob \{*.txt\} -contains \{Dear Margaret\}\\\\

11) If this is a budget CSV file, then let's pass that to our budget script for processing.
   --unix --exec \{budget.sh -f %file%\} -file -nocase -re \{^.*budget.*\.csv$\}\\\\

12) If the passed in file is actually a URL then attempt to download it.
   --unix --exec "wget -P $::env(HOME)/Downloads %filename% 2>/dev/null" -text -re \{^http.*\}\\\\


The configuration item then looks like:
RULES =\
	--stop -file -glob \{*.exe\}\\\\
	--stop -path -re \{\[\\\\\\\\/\]My Documents\[\\\\\\\\/\]\}\\\\
	--move ~/Documents/CV/cover_letters/ -nocase -file -re \{cover.?letter.*\.(docx?|txt)$\}\\\\
	--date --move ~/Backup/ -nocase -glob \{*.bak\}\\\\
	--delete -glob \{*~\}\\\\
	--delete %DEF_TRASH% -file -glob \{Anti-Vir.Dat\}\\\\
	--recurse -dir -glob \{Downloads\}\\\\
	--windows --move "$::env(HOME)/My Documents" -nocase -re \{\[.\]docx?$\}\\\\
	-- --delete -file -glob \{*.*\}\\\\
	--delete %DEF_TRASH% -file -glob \{*.txt\} -contains \{Dear Margaret\}\\\\
	--unix --exec \{budget.sh -f %file%\} -file -nocase -re \{^.*budget.*\.csv$\}\\\\
	--unix --exec "wget -P $::env(HOME)/Downloads %filename% 2>/dev/null" -text -re \{^http.*\}\\\\


=== Listing the currently set rules ===
 % drop-it --rules
Drop-It rules:
rule_id           action          checks                               match
      1           --stop          -file -name                          -glob \{*.exe\}
      2           --stop          -path                                -re \{\[\\\\\\\\/\]My Documents\[\\\\\\\\/\]\}
      3           --move          -file -nocase -name                  -re \{cover.?letter.*.(docx?|txt)$\}
      4           --date --move   -nocase -name                        -glob \{*.bak\}
      5           --delete        -name                                -glob \{*~\}
      6           --delete        -file -name                          -glob \{Anti-Vir.Dat\}
      7           --recurse       -dir -name                           -glob \{Downloads\}
      8           --move          -nocase -platform \{windows\} -name    -re \{\[.\]docx?$\}
      9           --delete        -file -name                          -glob \{*.*\}
     10           --delete        -file -name                          -glob \{*.txt\} -contains \{Dear Margaret\}
     11           --exec          -nocase -platform \{unix\} -file -name -re \{^.*budget.*.csv$\}
     12           --exec          -platform \{unix\} -text -name         -re \{^http.*\}


=== Using dry-run as a precaution ===
Testing the rule match with a dry run before actually allowing the script to make actions is always a good idea.
A rouge rule can cause all kinds of trouble. Confidence in the rules is gained over time.
 % drop-it -d letter.txt
     deleted letter.txt ==> /home/username/Trash (dry run: 9)

The number noted in the dry run is the rule that matched.


=== Requesting an explain output for a given rule ===
 % drop-it --explain --rule 9
Rule 9: Delete <input> to /home/username/Trash 
    + type matches -file
    + name matches -glob \{*.txt\}
    + text does contain \{Dear Margaret\}


=== Why did the document letter.doc not match this specific rule? ===
 % drop-it --explain --rule 9 letter.doc
Rule 9: Delete letter.doc to /home/username/Trash 
   + type matches -file
   - name does not match -glob \{*.txt\}
   + file does contain \{Dear Margaret\}

The explain output shows that the file name did not match *.txt.


=== Performance checking ===
 % drop-it letter.doc --time                   
Time report:
rule_id    avg/ms action          checks                               match
      1      0.00 --stop          -file -name                          -glob \{*.exe\}
      2      1.00 --stop          -path                                -re \{\[\\\\\\\\/\]My Documents\[\\\\\\\\/\]\}
      3      0.00 --move          -file -nocase -name                  -re \{cover.?letter.*.(docx?|txt)$\}
      4      0.00 --date --move   -nocase -name                        -glob \{*.bak\}
      5      1.00 --delete        -name                                -glob \{*~\}
      6      0.00 --delete        -file -name                          -glob \{Anti-Vir.Dat\}
      7      0.00 --recurse       -dir -name                           -glob \{Downloads\}
      8      0.00 --move          -nocase -platform \{windows\} -name    -re \{\[.\]docx?$\}
      9      0.00 --delete        -file -name                          -glob \{*.*\}
     10      1.00 --delete        -file -name                          -glob \{*.txt\} -contains \{Dear Margaret\}
     11      0.00 --exec          -nocase -platform \{unix\} -file -name -re \{^.*budget.*.csv$\}
     12      0.00 --exec          -platform \{unix\} -text -name         -re \{^http.*\}

Overall duration 0.003s for 12 rule checks across 1 file.

This will show if one rule takes up significantly more processing time than other rules.
The contains check for example may be slow when grepping through large files.


=== Cron job example ===

# Find files in downloads directory older than 7 days and pass them to drop-it
0 11 * * * find ~/Downloads -maxdepth 1 -mtime +7 -print0 | xargs -r0 ~/path/to/bin/drop-it --notify


=== Troubleshooting ===

Example errors:

In Tcl curly brackets, square brackets and quotes need to match.

 % drop-it --rules
Unable to parse rule "--delete %DEF_TRASH% -file -glob \{Anti-Vir\}?.Dat\}" due to brace mismatch.
Unable to parse rule "--recurse -dir -glob \{Downloads\}\}" due to brace mismatch.
Unable to parse rule "--delete %DEF_TRASH% -file -glob \{*.txt\} -contains \{Dear Margaret\}\]" due to brace mismatch.
Drop-It rules:
rule_id           action          checks                               match
      1           --stop          -file -name                          -glob \{*.exe\}
      2           --stop          -path                                -re \{\[\\/\]My Documents\[\\/\]\}
      3           --move          -file -nocase -name                  -re \{cover.?letter.*\.(docx?|txt)$\}
      4           --date --move   -nocase -name                        -glob \{*.bak\}
      5           --delete        -name                                -glob \{*~\}
      6           --move          -nocase -platform \{windows\} -name    -re \{\[.\]docx?$\}
      7           --delete        -file -name                          -glob \{*.*\}
      8           --exec          -nocase -platform \{unix\} -file -name -re \{^.*budget.*\.csv$\}
      9           --exec          -platform \{unix\} -text -name         -re \{^http.*\}

The above means that two of the rules have a brace mismatch. The error may happen if there are too many or too few square brackets (\]) or curly braces (\}).

Rules missing?

It might be that the trailing backslash (\\\\) needed by multi-line configuration items is missing.
}

if {[info exists argv]} {
	drop_it::init
	drop_it::main $argv
}

