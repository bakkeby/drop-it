# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require struct::queue 1.4.2 

package require tapp 1.0
package require notifications 1.0

package require drop_it_rule 1.0
package require drop_it_action 1.0
package require drop_it_stats 1.0

package provide drop_it 1.0

namespace eval drop_it {
	variable RULES
	variable RPT {}
	variable RULES_LOADED 0
	::struct::queue match_queue
}

proc drop_it::write_default_config { file } {
	file_utils::write_file $file {
#
# Drop-It configuration
#

# Log settings:
#    LOG_DIR     - the log directory
#    LOG_FILE    - the log file name (or stdout or stderr)
#    LOG_LEVEL   - the log level to log at, limits how much is logged
# LOG_FILE  = %Y-%m-%d_%H.drop_it.log
LOG_FILE  = stdout
LOG_LEVEL = ERROR

DEF_TRASH = [file join $::env(HOME) Trash]

# Automatically create the rules file if it does not already exist
AUTO_CREATE_RULES_FILE = 1

# Defines which rules configuration file to use
CFG_FILE = [this_dir]/drop_it_rules.cfg

# Enables stats keeping of rules performance across runs and adds additional
# information to the --rules output to indicate which rules are actually being
# used and which can be removed.
FUNC_STATS = 0

# Whether to use DBUS for desktop notifications or not. Requires dbus-tcl to be
# installed. See http://dbus-tcl.sourceforge.net/
DBUS_NOTIFICATION = 0

# List of config items not to output in full when listing all configuration
# items using the --show-config option
MASKED_CFG = HELP ARGS MOREARGS HELP_EXAMPLES

# Command to execute in order to generate a checksum (used for backup purposes)
CHECKSUM_CMD = [expr {$::tcl_platform(platform) == {unix} ? {md5sum "%file%"} : {md5.exe "%file%"}}]

#
# This defines the executables to execute in relation to the -contains search.
#
# The configuration items are on the form CONTAINS_EXEC_<FILE EXTENSION> where
# the file extension is in uppercase and dots are replaced by underscores.
#
# There are two placeholders:
#   %file%     - will be replaced by the file being processed
#   %contains% - will be replaced by the -contains expression for the rule
#
# The default command (typically grep) is defined with CONTAINS_EXEC_DEFAULT.
#
# For example for a .tar.gz file the configuration item under linux could be:
#    CONTAINS_EXEC_TAR_GZ = tar -ztf %file% | grep -Ei "%contains%"
#
# This is all configurable for cross platform purposes and to allow for
# customised searches through archive files.
#
CONTAINS_GREP = grep -Ei "%contains%"
CONTAINS_EXEC_DEFAULT = $CONTAINS_GREP %file%
# CONTAINS_EXEC_PDF     = pdftotext %file% - | $CONTAINS_GREP
# CONTAINS_EXEC_TAR     = tar -tf %file% | $CONTAINS_GREP
# CONTAINS_EXEC_TGZ     = tar -ztf %file% | $CONTAINS_GREP
# CONTAINS_EXEC_TAR_GZ  = $CONTAINS_EXEC_TGZ
# CONTAINS_EXEC_TAR_XZ  = tar -Jtf %file% | $CONTAINS_GREP
# CONTAINS_EXEC_TAR_BZ2 = tar -jtf %file% | $CONTAINS_GREP
# CONTAINS_EXEC_TAR_LZ  = tar --lzip -tf %file% | $CONTAINS_GREP
# CONTAINS_EXEC_7Z      = 7z l %file% | $CONTAINS_GREP
# CONTAINS_EXEC_ZIP     = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_GZ      = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_BZ      = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_BZ2     = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_XZ      = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_LZ      = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_ARJ     = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_RAR     = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_CAB     = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_CPIO    = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_RPM     = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_ISO     = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_DEB     = $CONTAINS_EXEC_7Z
# CONTAINS_EXEC_PPTM    = $CONTAINS_EXEC_7Z
}
}

proc drop_it::init {} {

	param register -r --rules\
		--body {
			drop_it::init_dependencies
			drop_it::load_rules
			drop_it::log_rules --print
		}\
		--priority 99\
		--comment {shows a list of the defined rules, combine with --verbose to also list destinations}
		
	param register -n --notify\
		--config NOTIFY\
		--comment {enables system notification of the drop-it report}
		
	param register {} --examples\
		--body {
			log STDOUT [cfg get HELP_EXAMPLES]
		}\
		--priority 99\
		--comment {prints a long list of example usage}
		
	param register -a --explain_all\
		--body {
			drop_it::init_dependencies
			drop_it::load_rules
			drop_it::explain_all_rules
		}\
		--comment {shows explain output for all rules}

	param register -e --explain\
		--config EXPLAIN_RULE_MATCH\
		--comment {explains how the given file matched a rule, if used in combination with --rule then non-matches are explained as well}
		
	param register -t --time\
		--config TIME\
		--comment {enables performance debugging, will print a time spent report when the drop-it job is finished}

	param register {} --no_action\
		--config ACTION_RULE_MATCH\
		--config_value 0\
		--morearg\
		--comment {do a real run, but don't perform actions (consider using dry-run instead)}

	param register {} {--rule <rule_id>}\
		--body {
			cfg lappend LIMIT_TO_RULES {*}$values
			log DEBUG {Limiting operations to specific rule: $values}
		}\
		--max_args 1\
		--min_args 1\
		--validation integer\
		--priority 40\
		--comment {limits match operations to specific rule_ids}

	param register {} --list_args\
		--config PRINT_ARGUMENTS\
		--morearg\
		--comment {prints the arguments passed to drop-it}
		
	# Overrides the TApp default config option as this is handled
	# in a special way in drop_it::load_rules.
	param register -c --config\
		--override\
		--config CFG_FILE\
		--max_args 1\
		--min_args 1\
		--disporder 60\
		--priority 10\
		--morearg\
		--log {Using $value as configuration file}\
		--comment {specify the configuration file to use}
}

proc drop_it::generate_dropit_suffix { { file {} } } {
	if {$file != {} && [cfg enabled CHECKSUM_DROPIT_SUFFIX 1] && [file exists $file] } {
		return _drop_it_[file_utils::get_checksum $file]
	}
	return _dropit_[expr {int(rand()*100000)}].bak
}

proc drop_it::init_dependencies {} {

	check::init
	rule::init
	stats::init
}

proc drop_it::load_rules {} {

	variable RULES_LOADED 
	if {$RULES_LOADED} {
		return
	}

	if {![cfg exists CFG_FILE]} {
		log WARN {Configuration item "CFG_FILE" does not exist, not loading rules.}
		return
	}
	
	set load_rules_db_file 0
	set save_rules_db_file 0

	set rule_file [cfg get CFG_FILE]

	if {![file exists $rule_file]} {
		generate_rule_file $rule_file
	}

	set ext [string tolower [file extension $rule_file]]
	
	if {$ext == {.db}} {
		set load_rules_db_file 1
		set rules_db_file $rule_file
	} elseif {$ext == {.cfg}} {
		# Loading the file
		set old_cfg_multi_line_separator [cfg get CFG_MULTI_LINE_SEPARATOR { }]
		cfg set CFG_MULTI_LINE_SEPARATOR [cfg get RULE_SEPARATOR \u00]
		if {[catch {cfg file $rule_file} msg]} {
			log ERROR {Error occurred reading rules configuration file: $msg}
			exit
		}
		cfg set CFG_MULTI_LINE_SEPARATOR $old_cfg_multi_line_separator
		log DEBUG {Loaded rules configuration file $rule_file}
		
		set rules_db_file [file rootname $rule_file].db
		
		# Option to save the parsed rules as a .db file and load that
		# instead of parsing the rules for each run. We still need to
		# load the configuration file above in case we have additional
		# configuration items that the rules themselves rely on.
		if {[cfg enabled PARSE_RULES_ON_CHANGE_ONLY]} {
			if {[file exists $rules_db_file]} {
				set cfg_mtime [file mtime $rule_file]
				set dbf_mtime [file mtime $rules_db_file]
				if {$dbf_mtime < $cfg_mtime} {
					log WARN {Rules configuration file has changed, moving .db file to trash}
					set output [action::move_action $rules_db_file [cfg get DEF_TRASH] -suffix]
					set save_rules_db_file 1
				} else {
					log DEBUG {Rules configuration file has not changed, using .db file instead}
					set load_rules_db_file 1
				}
			} else {
				set save_rules_db_file 1
			}
		}
	}
	
	if {$load_rules_db_file} {
		rule::load $rules_db_file
		log DEBUG {Loaded rules file $rules_db_file}
	} elseif {[cfg exists RULES]} {
		rule::parse_rules [cfg get RULES]
	}
	
	if {$save_rules_db_file} {
		log STDOUT {Saving rules file $rules_db_file}
		rule::save $rules_db_file
	}
	
	if {[cfg enabled FUNC_STATS 0]} {
		stats load
	}
	
	set RULES_LOADED 1
}

proc drop_it::log_match_report { RPT } {

	variable ::tcl_platform

	if {[llength $RPT] == 0} {
		return
	}

	# If we only have "recursed" matches, i.e. no actual file actions having taken
	# place, then don't show the report unless we are debugging.
	if {![log_level_enabled DEBUG] && [lsearch -index 0 -not $RPT recurse] == -1} {
		return
	}

	set prefix [string repeat { } [expr {[cfg get EXPLAIN_CHECK_INDENT 3] + 2}]]
	touch report
	
	foreach operation $RPT {
		foreach { action file dest output index } $operation {
			if {[cfg enabled EXPLAIN_RULE_MATCH] && ![cfg exists LIMIT_TO_RULES]} {
				rule::select_rule $index
				set report [concat $report [rule::explain $file]]
			}
			set verb [text_utils::edify $action]
			if {[string match -nocase --stop $action]} {
				set destination {}
			} elseif {$output != ""} {
				set destination " ==> [regsub -all {\\\\} $output {/}]"
			} elseif {$dest != {}} {
				set destination " ==> [regsub -all {\\\\} $dest {/}]"
			} else {
				set destination {}
			}

			if {[cfg enabled DRY_RUN]} {
				append destination " (dry run: $index)"
			} else {
				append destination " ($index)"
			}
			lappend report "${prefix}$verb [regsub -all {\\+} $file {/}]$destination"
		}
	}

	set output [join $report \n]
	log INFO {Drop-It report:\n$output}
	log VERBOSE {Drop-It report:}
	log STDOUT "$output\n"

	if {[cfg enabled NOTIFY]} {
		set rpt_count {}
		if {[llength $RPT] > 1} {
			set rpt_count " ([llength $RPT])"
		}

		switch -- [string toupper $tcl_platform(platform)] {
		UNIX {
			set icon [file join [this_dir] icons drop_it.svg]
		}
		default {
			set icon [file join [this_dir] icons drop_it.png]
		}
		}
		notifications::notify $icon "Drop-It Report${rpt_count}" $output {Drop-It}
	}
}

proc drop_it::log_rules args {

	variable TIME

	set print 0
	set time 0
	set overall_duration 0
	set overall_count 0
	set max_count 0

	foreach opt {print time} {
		if {[lsearch $args --$opt] > -1} {
			set $opt 1
		}
	}
	
	if {$time && ![cfg enabled TIME]} {
		return
	}

	set action_width 16
	if {[cfg enabled VERBOSE]} {
		set action_width 70
	}
	set report_format [subst {%7s%-4s%7s %-${action_width}s%-36s %s}]

	append report [format $report_format rule_id { *} avg/ms action checks match {} {}]\n

	foreach rule_id [rule::get_rules] {
		rule::select_rule $rule_id
		set actions [rule::get_rule action]

		if {[rule::get_rule status] != {A}} {
			prepend actions {-- }
		}

		clear checks match contains avg_ms

		foreach check_id [rule::get_rule check_ids] {
			lassign [check::to_string $check_id] check_type check_checks check_match check_contains
			foreach check $check_checks {
				if {[lsearch $checks $check] == -1} {
					lappend checks $check
				}
			}
		}

		# Splits normal check parameters and match parameters for display purposes
		regexp {^(.*?) +(-(?:re|glob|contains).+)$} [join $checks { }] - checks match

		set hash [rule::get_rule hash]
		lassign [stats get $hash] cr_date num_checks avg_ms num_matches last_match last_update

		set rating {}
		if {$last_update != {} && $cr_date != {}} {
			if {$last_match == {}} {
				set last_match $cr_date
			}
			# graceful run in time before reporting negative rules
			set grace_time [expr {86400 * [cfg get STAT_RUN_IN_DAYS 90]}]
		
			set grace_pct [expr {$grace_time * 100 / double($last_update - $cr_date)}]
			if {$grace_pct > 50} {
				set grace_pct 50
			}
			set match_age_percent [expr {($last_match - $cr_date) * 100 / (1 + $last_update - $cr_date) + $grace_pct}]
			if {$match_age_percent >= 80} {
				set rating { ***}
			} elseif {$match_age_percent >= 70} {
				set rating { **}
			} elseif {$match_age_percent >= 60} {
				set rating { *}
			} elseif {$match_age_percent >= 40} {
				set rating {}
			} elseif {$match_age_percent >= 30} {
				set rating { -}
			} elseif {$match_age_percent >= 20} {
				set rating { --}
			} else {
				set rating { ---}
			}
		}
		if {$avg_ms == 0} {
			set avg_ms {}
		} else {
			set avg_ms [format {%.2f} $avg_ms]
		}
		
		if {$time && [info exists TIME($rule_id)]} {
			set duration $TIME($rule_id)
			incr overall_duration $duration
			set count $TIME($rule_id,count)
			incr overall_count $count
			if {$count > $max_count} {
				set max_count $count
			}
		
			set avg_ms [format {%.2f} [expr {double($duration)/double($count)}]]
		}

		append report [format $report_format $rule_id $rating $avg_ms $actions $checks $match]\n
	}

	if {$time} {
		set file_desc  [expr {$max_count == 1 ? {file} : {files}}]
		set check_desc [expr {$overall_count == 1 ? {check} : {checks}}]
		append report \n[subst {Overall duration [expr {$overall_duration/1000.00}]s for $overall_count rule $check_desc across $max_count $file_desc.}]\n
	}

	set header [expr {$time ? {Time report:} : {Drop-It rules:}}]
	log INFO   {$header\n$report}
	log STDOUT {$header\n$report}
}

proc drop_it::purge_void_stats {} {

	set hashes [stats keys]
	foreach rule_id [rule::get_all_rules] {
		rule::select_rule $rule_id
		set hash [rule::get_rule hash]
		set hashes [lremove $hashes $hash]
	}
	
	foreach hash $hashes {
		log INFO {Removing void hash $hash}
		stats remove $hash
	}
}

proc drop_it::main { argv } {

	if {[llength $argv] < 1} {
		set argv --help
	}
	
	param parse $argv remaining_args

	if {[cfg enabled PRINT_ARGUMENTS]} {
		log STDOUT {Application arguments were: $argv}
	}
	
	# Initialise based on config file
	if {[catch {init_dependencies} msg]} {
		log STDOUT {There was a problem initialising due to: $msg}
		log ERROR  {There was a problem initialising due to: $msg}
		exit
	}
	
	param separate $remaining_args app_args files dirs

	foreach arg $app_args {
		switch -regexp -- $arg {
		{^[A-Za-z]+://.*$} {
			# Allow URL's to be passed in.
			log DEBUG {Processing a non-file}
			lappend files $arg
		}
		default {
			log ERROR {Cannot process non-existing file $arg}
		}
		}
	}

	# If there are no application specific arguments or files to process
	# then don't continue processing.
	if {[llength $files] || [llength $dirs]} {
		process $files $dirs
	} elseif {[cfg exists LIMIT_TO_RULES] && [cfg enabled EXPLAIN_RULE_MATCH]} {
		drop_it::load_rules
		drop_it::explain_all_rules
	}
}

proc drop_it::process { files dirs } {

	variable RPT
	variable RULES

	drop_it::load_rules

	if {[cfg enabled LOG_RULES]} {
		log_rules
	}

	process_files [concat $files $dirs]
	touch unmatched_files
	
	if {[cfg enabled ACTION_RULE_MATCH 1]} {
		set unmatched_files [drop_it::action_match]
	}

	if {[cfg enabled FUNC_STATS 0]} {
		update_stats
	}
	
	if {[llength $RPT] > 0 || [cfg enabled TIME]} {
		drop_it::log_match_report $RPT
		drop_it::log_rules --time
	}

	if {[llength $unmatched_files] > 0} {
		set unmatched_files [join $unmatched_files \n]
		log INFO   {Drop-It reported no match on the file(s) provided:\n$unmatched_files}
		if {[tapp_log::log_level_enabled DEBUG]} {
			log STDOUT {Drop-It reported no match on the file(s) provided: \n$unmatched_files}
		} elseif {[tapp_log::log_level_enabled VERBOSE]} {
			log STDOUT {Drop-It reported no match on some of the files provided, perhaps create a new rule?\n}
		}
	}
	
	if {[cfg enabled FUNC_STATS 0] && [stats dirty]} {
		drop_it::purge_void_stats
		stats save
	}
}

proc drop_it::update_stats {} {

	variable TIME
	
	foreach rule_id [rule::get_rules] {
		if {![info exists TIME($rule_id)]} {
			continue
		}
		rule::select_rule $rule_id
		
		set hash [rule::get_rule hash]
		lassign [stats get $hash] cr_date num_checks avg_ms num_matches last_match last_update

		set avg_ms [expr {(double($avg_ms) * $num_checks + double($TIME($rule_id))) / double($num_checks + $TIME($rule_id,count))}]
		incr num_checks $TIME($rule_id,count)
		set last_update [clock seconds]
		
		stats set $hash [list $cr_date $num_checks $avg_ms $num_matches $last_match $last_update]
	}
}

proc drop_it::print_debug_info {} {
	puts { *** Printing debug information ***}
	foreach type {check rule action destination} {
		puts " *** [string totitle $type] array: "
		${type}::print_array
	}
	# flag print
	# type print
	puts { *** Configuration: }
	puts [cfg print]
}

# Reset all drop-it arrays and configuration, used for testing purposes.
proc drop_it::reset {} {

	variable RPT
	catch {unset RPT}
	set RPT {}
	cfg reset
	tapp_log::reset
	foreach type {check rule} {
		${type}::reset
	}
	flag reset
	type reset
	destination reset
	action reset
}

proc drop_it::process_files { files } {
	foreach file $files {
		if {[catch {process_file $file} msg]} {
			set file_type [file_utils::file_type $file]
			log ERROR  {Error processing $file_type: $msg}
			log STDOUT {Error processing $file_type: $msg}
		}
	}
}

proc drop_it::process_file { input } {

	# A blank / null rule_id means here that there were no match on the file.
	# We still add this to the match_queue so that we can identify files that
	# had no match.
	log DEBUG {Processing $input}
	set rule_id [drop_it::get_rule_match $input]
	rule::select_rule $rule_id
	match_queue put [list $input $rule_id]
}

proc drop_it::explain_all_rules { { input "<input>" } } {

	touch report
	foreach rule_id [rule::get_rules] {
		rule::select_rule $rule_id
		lappend report [join [rule::explain $input] {\n }] {}
	}
	if {$report != {}} {
		set output [join $report \n]
		log INFO {Drop-It explain:\n$output}
		log VERBOSE {Drop-It explain:}
		log STDOUT "$output"
	}
}

proc drop_it::action_match {} {

	variable RPT

	set unmatched_files {}

	while {[match_queue size] > 0} {
		lassign [match_queue get] input rule_id
		if {$rule_id != {}} {
			rule::select_rule $rule_id
			set RPT [concat $RPT [rule::action $input]]
			set hash [rule::get_rule hash]
			lassign [stats get $hash] cr_date num_checks avg_ms num_matches last_match last_update
			set last_match  [clock seconds]
			set last_update [clock seconds]
			incr num_matches 1
			stats set $hash [list $cr_date $num_checks $avg_ms $num_matches $last_match $last_update]
		} else {
			lappend unmatched_files $input
		}
	}
	return $unmatched_files
}

proc drop_it::get_rule_match { input } {

	variable TIME
	array set CHECK_RES {}

	set explain_rule_match 0
	if {[cfg enabled EXPLAIN_RULE_MATCH] && [cfg exists LIMIT_TO_RULES]} {
		set explain_rule_match 1
		set indent [string repeat { } [cfg get EXPLAIN_CHECK_INDENT 3]]
	}

	foreach rule_id [rule::get_rules] {

		set start_time [clock milliseconds]

		set rule_matched 1
		rule::select_rule $rule_id

		if {$explain_rule_match} {
			set explain [rule::explain $input 0]
		}

		if {[rule::is_active $rule_id]} {

			set check_ids [rule::get_rule check_ids]

			# Running through rule checks are done here to keep track of which
			# checks have been verified already (as a performance optimisation).
			# It would be possible to move this to RULE, but that would lead to
			# it having the CHECK_RES thus needing to keep track of the files
			# that has been processed (actually just the previous file, assuming
			# the one file runs through all rules in order).
			clear running_through_check_ids
			if {[cfg enabled VERBOSE]} {
				set running_through_check_ids " Running through checks ($check_ids)"
			}
			log DEBUG {Rule $rule_id:$running_through_check_ids}

			foreach check_id $check_ids {
				set check_key $input,check_$check_id
				if {![info exists CHECK_RES($check_key)]} {
					set CHECK_RES($check_key) [check::check $check_id $input]
				}
				
				log DEBUG {   [check::explain $check_id $input $CHECK_RES($check_key)]}

				if {$explain_rule_match} {
					lappend explain $indent[join [check::explain $check_id $input $CHECK_RES($check_key)] { }]
				}

				if {!$CHECK_RES($check_key)} {
					set rule_matched 0
					if {!$explain_rule_match} {
						log DEV {Rule $rule_id: $input did not match (skipping rule)}
						break
					}
				}
			}
		} else {
			if {$explain_rule_match} {
				lappend explain $indent[join {- rule is suspended} { }]
			}
			set rule_matched 0
		}

		if {$explain_rule_match} {
			set output [join $explain \n]
			log INFO {Drop-It explain:\n$output}
			log VERBOSE {Drop-It explain:}
			log STDOUT "$output\n"
		}

		set duration [expr {[clock milliseconds] - $start_time}]
		incr TIME($rule_id) $duration
		incr TIME($rule_id,count)

		if {$rule_matched} {
			if {[tapp_log::log_level_enabled DEBUG]} {
				set file_type [file_utils::file_type $input]
				log DEBUG {     $file_type $input matches rule $rule_id}
			}
			return $rule_id
			break
		}
	}
	log DEBUG {No rule match found for $input}
	return
}

proc drop_it::generate_rule_file { rule_file } {
	set msg {Warning: The rules configuration file $rule_file does not exist\n}
	log STDOUT $msg
	log WARN $msg
	if {![cfg AUTO_CREATE_RULES_FILE 1]} {
		return
	}
	log STDOUT {Generating empty rules file $rule_file\n}
	lappend rule_file_content ${::rule::rule_configuration_desc}[format {%-8s%-11s%-16s%-59s%-29s%-40s} # Limit Action Destination Checks Match]
	lappend rule_file_content [subst {RULES =\\}]
	lappend rule_file_content [format {%-8s%-11s%-16s%-59s%-29s%-40s} {} {} --stop {} -file "-glob {*.*}\\"]
	lappend rule_file_content {} {}
	file_utils::write_file $rule_file [join $rule_file_content \n]
}

