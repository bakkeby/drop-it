# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package provide drop_it_stats 1.0

namespace eval stats {
}


proc stats::init {} {

	variable DIRTY

	set pname [opt::proc ::stats --preset array]
	opt::option $pname get --proc [list stats::get]
	
	set cfg_file [cfg get CFG_FILE]
	set cfg_root [file rootname $cfg_file]
	set hostname [info hostname]
	set stat_ext [cfg get STATS_EXTENSION .stat]

	# Tests ended up writing a .stat file in the
	# current directory, so avoiding initialisation
	# if we don't have a config file.
	if {$cfg_root != {}} {
		::stats init ${cfg_root}.${hostname}${stat_ext}
	}
	
	set DIRTY 0
}

proc stats::get {key} {

	if {[info exists ::STATS($key)]} {
		return $::STATS($key)
	}
	
	#      cr_date num_checks avg_ms num_matches last_match last_update
	return [list [clock seconds] 0 0 0 {} {}]
}

