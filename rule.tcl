# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0
package require drop_it_check 1.0
package require drop_it_action 1.0
package require drop_it_destination 1.0

package provide drop_it_rule 1.0

namespace eval rule {

	variable RULE
	variable current_rule_id
	variable rule_configuration_desc
	
	set pname [opt::proc store --preset array]

	set RULE(rule_ids) {}
	set current_rule_id {}
}

set ::rule::rule_configuration_desc [subst {
#
# Drop-It rules configuration
#
# Rules are defined by a series of action and check parameters and are separated by newlines.
# Multi-line configuration items are defined by a trailing backslash \.
#
# Optional parameters:
#   --rule <id>               - the specific ID to set / update
#   --status <status>         - 'A'ctive or 'S'uspended
#   -- or --suspended         - suspend the action, equivalent to --status S
#   #                         - commented out, different from --suspended in that the rule will
#                               not be available within drop-it, i.e. not included when running
#                               through checks or listing rules with the --rules option
#
# Actions:
#   --move <destination>      - moves a matching file or directory to the given destination
#   --delete <destination?>   - removes the matching file or directory if destination is
#                               empty, otherwise equivalent to --move
#   --exec <destination>      - passing a matching file or directory to a destination script
#   --recurse                 - process any subdirectories or files within a matching directory
#                               Automatically deletes directory after processing if empty
#                               This behaviour can be configured off with the config item
#                               DELETE_DIRECTORY_IF_EMPTY_AFTER_RECURSE
#   --rename <rename list>    - renames a given file based on a list of match rename pairs of
#                               regular expressions. The matching defaults to be case
#                               insensitive, but can be made case sensitive globally by setting
#                               the configuration item CASE_SENSITIVE_RENAME_ACTION. If multiple
#                               pairs of rename matches are provided then they are processed in
#                               the order passed in. It is recommended to set long lists as a
#                               configuration item and set the destination to be %CONFIG_NAME%
#                               example list: {{^(prefix_)} {new_\1}}
#                               (i.e. files starting with prefix are renamed to new_prefix_*)
#   --stop                    - action to stop further processing if match is found
#   --date                    - prepend the file with the file date
#
# Check arguments:
#   -case                     - indicates that the match check is case sensitive
#   -nocase                   - indicates that the match check is case insensitive
#   -invert                   - indicates that we are doing negative matching, e.g. match on
#                               not a match
#
# Checks:
#   -dir                      - input is a directory
#   -file                     - input is a file
#   -link                     - input is a symlink
#   -text                     - input is not a file (plain text)
#   -socket                   - input is a socket
#   -pipe (or -fifo)          - input is a named pipe
#   -block                    - input is a block special file
#   -character                - input is a character special file
#   -writable                 - checks whether we have file write permission
#   -readable                 - checks whether we have file read permission
#   -executable               - checks whether we have file executable permission
#   -newer <relative date>    - check whether a file is newer than a relative date, for
#                               example "6 days" or "24 hours".
#                               See http://www.tcl.tk/man/tcl8.5/TclCmd/clock.htm#M22
#   -older <relative date>    - as above, check whether a file is older than a relative date
#   -smaller <size>           - check whether a file is smaller than a given size, for
#                               example "24KB", "1.2MB", or "16384 bytes"
#                               warning: calculating the total size of a large directory may
#                               be of performance concern
#   -larger <size>            - as above, check whether a file is larger than a given size
#   -windows                  - rule is only active on windows platforms
#   -unix                     - rule is only active on unix platforms
#   -mac                      - rule is only active on macintosh platforms
#   -re <regexp>              - match a regular expression
#   -glob <glob>              - match a glob pattern, e.g. *.txt
#   -name                     - indicates that regexp/glob match should be done on file name [expr {[cfg get DEFAULT_MATCH_CHECK name] == {name} ? {(default)} : {}}]
#   -path                     - indicates that regexp/glob match should be done on file path [expr {[cfg get DEFAULT_MATCH_CHECK name] == {path} ? {(default)} : {}}]
#   -contains <regexp>        - file contains text (or files) matching regular expression or
#                               directory contains file names matching regular expression.
#                               the contains check is configurable on a per file extension
#                               basis, see drop_it.cfg for more details
#   -is_empty                 - check for empty directory or zero-sized file
#   -exists <destination>     - check whether the destination exists, typically used to
#                               check whether a removable drive is present or not
#   -not_exists <destination> - check that the destination does not exist
#
}]
#
# Notes on adding new checks:
#   - add tests in check.test
#   - add tests in drop_it.test
#   - add parameter details to this proc
#   - describe check the comment above
#   - add check procs (named check::check_<check>) in check.tcl
#   - if the new checks are of performance concern, consider changing
#     the check order in check::init in check.tcl
#   - add check to proc check::to_string in check.tcl
#   - if adding a new parameter type check that it is captured in proc
#     rule::add in rule.tcl
#
# Notes on adding new actions:
#   - add tests in action.test
#   - add tests in drop_it.test
#   - add parameter details to this proc
#   - describe action the comment above
#   - add action procs in action.tcl
#
proc rule::init {} {

	variable INIT
	variable PARAMS

	if {[info exists INIT] && $INIT == 1} {
		return
	}

	# Optional parameters:
	#          name          regexp                  num_args  default  param_type
	set PARAMS(--rule)      {{^--?rule$}             1         {}       rule}
	set PARAMS(--status)    {{^--?status$}           1         A        status}
	set PARAMS(--suspended) {{^--?susp(ended)}       0         S        status}
	set PARAMS(#)           {{^(#|--)$}              0         S        status}

	# Actions
	#          name          regexp                  num_args  default  param_type
	set PARAMS(--move)      {{^--?move$}             1         {}       action}
	set PARAMS(--delete)    {{^--?del(ete)?$}        1         enabled  action}
	set PARAMS(--exec)      {{^--?exec(ute)?$}       1         {}       action}
	set PARAMS(--recurse)   {{^--?recurse$}          1         {f d r}  action}
	set PARAMS(--rename)    {{^--?rename$}           1         {}       action}
	set PARAMS(--stop)      {{^--?stop$}             0         {}       action}
	set PARAMS(--date)      {{^--?date$}             0         {}       action}

	# Check arguments:
	#          name          regexp                  num_args  default  param_type
	set PARAMS(-case)       {{^--?case$}             0         {}       check_flag}
	set PARAMS(-nocase)     {{^--?nocase$}           0         {}       check_flag}
	set PARAMS(-inverse)    {{^--?inverse$}          0         {}       check_flag}

	# Checks:
	#          name          regexp                  num_args  default  param_type
	set PARAMS(-type)       {{^--?type$}             1         {}       type}
	set PARAMS(-dir)        {{^--?dir(ectory)?$}     0         D        type}
	set PARAMS(-file)       {{^--?file$}             0         F        type}
	set PARAMS(-socket)     {{^--?socket$}           0         S        type}
	set PARAMS(-block)      {{^--?block$}            0         B        type}
	set PARAMS(-character)  {{^--?character$}        0         C        type}
	set PARAMS(-link)       {{^--?(sym)?link$}       0         L        type}
	set PARAMS(-text)       {{^--?text$}             0         T        type}
	set PARAMS(-fifo)       {{^--?(pipe|fifo)$}      0         P        type}
	set PARAMS(-writable)   {{^--?writable$}         0         {}       check}
	set PARAMS(-readable)   {{^--?readable$}         0         {}       check}
	set PARAMS(-executable) {{^--?executable$}       0         {}       check}
	set PARAMS(-newer)      {{^--?newer$}            1         {}       check}
	set PARAMS(-older)      {{^--?older$}            1         {}       check}
	set PARAMS(-smaller)    {{^--?smaller$}          1         {}       check}
	set PARAMS(-larger)     {{^--?larger$}           1         {}       check}
	set PARAMS(-is_empty)   {{^--?is_?empty$}        0         {}       check}
	set PARAMS(-exists)     {{^--?exists$}           1         {}       check}
	set PARAMS(-not_exists) {{^--?not_exists$}       1         {}       check}
	set PARAMS(-windows)    {{^--?win(dows)?$}       0         {}       check}
	set PARAMS(-unix)       {{^--?(unix|linux)$}     0         {}       check}
	set PARAMS(-mac)        {{^--?mac(intosh)?$}     0         {}       check}
	set PARAMS(-java)       {{^--?(java|os400)$}     0         {}       check}
	set PARAMS(-contains)   {{^--?contains$}         1         {}       check}
	set PARAMS(-name)       {{^--?name$}             0         {}       match_flag}
	set PARAMS(-path)       {{^--?path$}             0         {}       match_flag}
	set PARAMS(-re)         {{^--?reg?(exp?)?$}      1         {}       match}
	set PARAMS(-glob)       {{^--?glob$}             1         {}       match}

	# Option to supply custom rule definitions, might be dangerous.
	array set PARAMS [cfg get CUSTOM_RULE_PARAM_OVERRIDE {}]

	# Define the order of the parameter types that need to be processed before other types.
	# For example the check_flag arguments which indicates whether a check is supposed to
	# be case insensitive or not have to take place before we process the checks that
	# depend on these flags. The rule specifier needs to be the very first that we process
	# when adding rules. Any undefined types will be appended to this list.
	set param_types [list rule check_flag action_flag match_flag type match action]
	touch {*}$param_types

	# Action types need specific sorting as the order determines in which order the actions
	# are executed when a rule contains multiple actions.
	foreach action_type [list --exec --date --rename --move --stop --recurse --delete] {
		if {[info exists PARAMS($action_type)]} {
			lappend action $action_type
		}
	}

	# Parse the rest of the parameters in a generic way.
	foreach name [array names PARAMS] {
		set param_type [lindex $PARAMS($name) 3]
		if {![info exists $param_type] || [lsearch [set $param_type] $name] == -1} {
			lappend $param_type $name
		}
		if {[lsearch -exact $param_types $param_type] == -1} {
			lappend param_types $param_type
		}
	}

	log DEV {rule::init: parameter types = $param_types}
	set PARAMS(parameters) [subst [join $$param_types { $}]]
	log DEV {rule::init: PARAMS(parameters) = $PARAMS(parameters)}
	set INIT 1
}

proc rule::save { rules_db_file } {
	store init $rules_db_file
	store reset
	foreach type {check rule} {
		store set $type [${type}::get_array]
	}
	foreach type {flag type destination action} {
		store set $type [${type} unload]
	}
	store save
	log INFO {Saved rules db file: $rules_db_file}
}

proc rule::load { rule_file } {
	store init $rule_file
	foreach type {check rule} {
		${type}::set_array [store get ${type}]
	}
	foreach type {flag type destination action} {
		${type} load [store get ${type}]
	}
}

proc rule::parse_rules { rules } {
	foreach rule [split $rules [cfg get RULE_SEPARATOR \u00]] {
		set rule [string trim $rule]
		if {$rule == {}} {
			continue
		}
		if {![string is list $rule]} {
			log WARN {Unable to parse rule "$rule" due to brace mismatch.}
			log STDOUT {Unable to parse rule "$rule" due to brace mismatch.}
		} else {
			add {*}$rule
		}
	}
}

#
# Parses and adds rules based on the rule parameter definitions.
#
# See rule::init for details.
#
proc rule::add args {

	if {[llength $args] == 0} {
		return
	}

	log DEV {rule::add called with args: $args}
	touch check_flags action_flag types

	set match_check [cfg get DEFAULT_MATCH_CHECK name]

	variable PARAMS
	foreach param $PARAMS(parameters) {
		set value [_get_param $args $param]
		if {$value == {} && $param != {--rule}} {
			continue
		}
		if {$value == {enabled}} {
			set value {}
		}
		lassign $PARAMS($param) - - - param_type
		set check_type [string trimleft $param -]

		# Certain parameter types needs to be dealt with in a specific manner
		switch -- $param_type {
		rule {
			# Expected to be the first parameter type that is checked
			set rule_id $value
			if {$rule_id == {} || [rule::exists $rule_id]} {
				set rule_id [rule::_get_next_rule_id]
			}

			rule::select_rule $rule_id
			rule::set_rule status [cfg get DEFAULT_RULE_STATUS A]
		}
		check_flag {
			lappend check_flags $param
		}
		action_flag {
			lappend action_flags $param
		}
		match_flag {
			# A match flag of path indicates that we should do a path check when
			# adding regexp or glob matching, otherwise match on file name.
			set match_check [string trimleft $param -]
		}
		check {
			rule::set_rule $param_type $check_type $value $check_flags
		}
		action {
			rule::set_rule $param_type $check_type $value {$action_flag}
		}
		type {
			lappend types {*}[string toupper $value]
		}
		match {
			rule::set_rule check $match_check $value [concat $check_flags -$check_type]
		}
		default {
			rule::set_rule $param_type $value
		}
		}
	}

	# Finally add the type check (if relevant)
	# A sorted list prevents duplicates when adding type checks
	set types [join [lsort $types] {}]
	if {$types != {}} {
		rule::set_rule check type $types
	}
	
	lappend ::rule::RULE(rule_ids) $rule_id
}

proc rule::_get_param { param_list param } {

	variable PARAMS

	set param_checks [lindex [array get PARAMS $param] 1]
	if {$param_checks == {}} {
		log WARN {rule::_get_param - parameter $param not found}
		return
	}

	lassign $param_checks regex num_args default_value

	set index [lsearch -nocase -regexp $param_list $regex]
	if {$index == -1} {
		return
	}

	touch value
	for {set i 1} {$i < $num_args + 1} {incr i} {
		set next [lindex $param_list [expr {$index + $i}]]
		# If the next parameter also looks like a parameter flag instead
		# of a parameter value, then we have to presume the value has not
		# been provided
		if {[string match -* $next]} {
			break
		}
		
		if {$num_args > 1} {
			lappend value $next
		} else {
			set value $next
		}
	}

	if {$value == {}} {
		set value $default_value
	}

	if {$value == {} && $num_args == 0} {
		set value {enabled}
	}
	return $value
} 

proc rule::action { input } {

	variable RULE
	variable current_rule_id

	set rule_id $current_rule_id

	# If rule_id is empty it means there were no rules that matched
	# the input, thus no action to be executed.
	if {$rule_id == {}} {
		return
	}

	touch RPT
	set action_ids [rule::get_rule action_id]
	foreach action_id $action_ids {
		set action         [string tolower [::action get $action_id]]
		set action         [string trimleft $action {-}]
		set destination_id [::action destination_id $action_id]
		set destination    [destination subst $destination_id $input]
		set flags          {}

		if {![catch {set output [action::${action}_action $input $destination $flags]} msg]} {
			lassign $output result_code dest
			set output $dest
			set actioned $action
			if {$result_code == {NOT_OK}} {
				append actioned { fail}
			}

			# If the directory has changed, then only set the directory as the destination.
			if {$dest != {}} {
				if {[file tail $input] == [file tail $dest]} {
					set output [file dirname $dest]
				}
			}
			
			if {[lsearch [cfg get NO_REPORT_ACTIONS {stop}] $action] == -1
				|| [tapp_log::log_level_enabled DEBUG]
				|| [cfg enabled VERBOSE]} {
				lappend RPT [list $actioned $input $destination $output $rule_id]
			}

			if {$result_code != "OK"} {
				break
			}

			# If the input has changed as part of this action
			if {$dest != {} && $dest != $input} {
				log DEBUG {Overriding input file "$input" with "$dest"}
				set input $dest
			}
		} else {
			set actioning [text_utils::ingify $action]
			log ERROR {Error $actioning $input ==> $destination due to $msg}
			break
		}
	}
	return $RPT
}

# Returns the next available rule ID (serial)
proc rule::_get_next_rule_id {} {

	variable RULE

	incr RULE(idx)

	if {[rule::exists $RULE(idx)]} {
		return [rule::_get_next_rule_id]
	}

	return $RULE(idx)
}

proc rule::select_rule { rule_id } {

	variable current_rule_id

	set current_rule_id $rule_id
	log DEV {Changed to rule_id $rule_id}
}

proc rule::exists { rule_id } {

	variable RULE

	if {[info exists RULE(rule_ids)] && [lsearch $RULE(rule_ids) $rule_id] > -1} {
		return 1
	}

	return 0
}

proc rule::print_array {} {

	variable RULE

	if {[catch {parray RULE} msg] && ![regexp {error writing "std(out|err)": broken pipe} $msg]} {
		puts stderr $msg
	}
}

proc rule::reset {} {

	variable RULE

	array unset RULE
	set RULE(rule_ids) {}
}

proc rule::get_array {} {

	variable RULE

	return [array get RULE]
}

proc rule::set_array { rule_list } {

	variable RULE

	array set RULE $rule_list
}

proc rule::is_active { rule_id } {

	variable RULE

	if {$rule_id == {} || ![rule::exists $rule_id]} {
		return 0
	}

	return [expr {$RULE($rule_id,status) == {A}}]
}

# Returns currently relevant rules, may be limited to a specific set.
proc rule::get_rules {} {

	variable RULE

	touch order_list rule_list

	foreach rule_id [expr {[cfg exists LIMIT_TO_RULES] ? [cfg get LIMIT_TO_RULES] : $RULE(rule_ids)}] {
		incr idx
		lappend order_list [list $rule_id [nvl {$RULE($rule_id,order)} $idx]]
	}
	foreach order [lsort -integer -index 1 $order_list] {
		lappend rule_list [lindex $order 0]
	}

	return $rule_list
}

# Returns all rules within the system
proc rule::get_all_rules {} {

	variable RULE
	
	return $RULE(rule_ids)
}

proc rule::explain { input {include_checks 1}} {

	variable PARAMS
	variable current_rule_id

	set rule_id $current_rule_id

	if {![rule::exists $rule_id]} {
		return [list [format {Rule %s does not exist} $rule_id]]
	}

	touch output destination actions

	set action_ids [rule::get_rule action_id]
	if {[llength $action_ids] == 0} {
		lappend output [format {Rule %s: %s} $rule_id $input]
	}

	foreach action_id $action_ids {
		set action [::action get $action_id]
		set destination_id [::action destination_id $action_id]
		set destination [list [destination subst $destination_id $input]]

		set num_args [lindex $PARAMS(--$action) 1]
		set action_disp [string totitle [string map {-- {}} $action]]
		set action_disp [string map {Exec Execute} $action_disp]
		if {$destination == {{}} || $num_args == 0} {
			lappend output [format {Rule %s: %s %s} $rule_id $action_disp $input]
		} elseif {[string match -nocase {exec} $action]} {
			lappend output [format {Rule %s: %s %s} $rule_id $action_disp $destination]
		} else {
			lappend output [format {Rule %s: %s %s to %s } $rule_id $action_disp $input $destination]
		}
	}

	if {$include_checks} {
		set check_indent [cfg get EXPLAIN_CHECK_INDENT 3]
		foreach check_id [rule::get_rule check_ids] {
			lappend output [string repeat { } $check_indent][join [check::explain $check_id $input 1] { }]
		}
	}
	return $output
}

proc rule::lookup { name } {

	variable RULE

	if {[info exists RULE($name)]} {
		return $RULE($name)
	}
}

proc rule::get_rule { option } {

	variable RULE
	variable current_rule_id

	touch output

	switch -- $option {
	action_id {
		if {[info exists RULE($current_rule_id,action_id)]} {
			set output $RULE($current_rule_id,action_id)
		}
	}
	action {
		foreach action_id [rule::get_rule action_id] {
			lappend output --[::action get $action_id]
			if {[cfg enabled VERBOSE]} {
				set dest [::action destination $action_id]
				if {$dest != {}} {
					lappend output $dest
				}
			}
		}
	}
	check_ids {
		set check_ids {}
		if {[info exists RULE($current_rule_id,check_ids)]} {
			set check_ids $RULE($current_rule_id,check_ids)
		}
		set output [check::get_optimal_check_order $check_ids] 
	}
	hash {
		if {![info exists RULE($current_rule_id,hash)]} {
			set hashstr [get_rule action]
			foreach check_id [rule::get_rule check_ids] {
				append hashstr [check::to_string $check_id]
			}
			set hash [::md5::md5 -hex $hashstr]
			set RULE($current_rule_id,hash) $hash
		}
		set output $RULE($current_rule_id,hash)
	}
	status -
	desc -
	default {
		if {[info exists RULE($current_rule_id,$option)]} {
			set output $RULE($current_rule_id,$option)
		}
	}
	}

	return $output
}



proc rule::set_rule { option value { param {} } { flags {} } } {

	variable RULE
	variable current_rule_id

	log DEV {Setting rule $current_rule_id $option $value $param $flags}

	switch -- $option {
	action_id {
		lappend RULE($current_rule_id,action_id) $value
	}
	action {
		rule::set_rule action_id [::action add $value $param $flags]
	}
	check_id {
		lappend RULE($current_rule_id,check_ids) $value
	}
	check {
		rule::set_rule check_id [check::add_check $value $param $flags]
	}
	name {
		set RULE($current_rule_id,name) $value
		set RULE($value) $current_rule_id
	}
	status -
	desc -
	default {
		set RULE($current_rule_id,$option) $value
	}
	}
}