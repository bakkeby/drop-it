# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0

package provide drop_it_destination 1.0

namespace eval destination {
	
	set pname [opt::proc ::destination --preset list --unique]
	opt::option $pname subst --proc [list destination::subst_destination]
}

proc destination::subst_destination { destination_id input } {

	set file_type [file_utils::file_type $input]
	if {$file_type == {text}} {
		set file_absolutepath $input
		set file_name $input
		set file_time [clock seconds]
	} else {
		set file_absolutepath [file_utils::get_absolute_path $input]
		set file_name [file tail $input]
		set file_time [file_utils::determine_file_time $input]
	}
	
	lappend replace_list %file%     [string map {\\ / { } {\ }} $file_absolutepath]
	lappend replace_list %filename% [string map {{ } {\ }} $file_name]
	lappend replace_list %rootname% [string map {{ } {\ }} [file rootname $file_name]]
	lappend replace_list %dirname%  [string map {\\ / { } {\ }} [file dirname $input]]
	lappend replace_list %Y%        [clock format $file_time -format %Y]
	lappend replace_list %m%        [clock format $file_time -format %m]
	lappend replace_list %d%        [clock format $file_time -format %d]

	set destination [destination get $destination_id]
	set destination [cfg subst $destination]
	set destination [string map $replace_list $destination]

	return $destination
}