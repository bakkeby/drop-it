# Tcl package index file, version 1.1
# This file is generated by the "pkg_mkIndex" command
# and sourced either when an application starts up or
# by a "package unknown" script.  It invokes the
# "package ifneeded" command to set up package-related
# information so that packages will be loaded automatically
# in response to "package require" commands.  When this
# script is sourced, the variable $dir must contain the
# full path name of this file's directory.

package ifneeded drop_it 1.0 [list source [file join $dir drop_it_pkg.tcl]]
package ifneeded drop_it_action 1.0 [list source [file join $dir action.tcl]]
package ifneeded drop_it_check 1.0 [list source [file join $dir check.tcl]]
package ifneeded drop_it_destination 1.0 [list source [file join $dir destination.tcl]]
package ifneeded drop_it_flag 1.0 [list source [file join $dir flag.tcl]]
package ifneeded drop_it_rule 1.0 [list source [file join $dir rule.tcl]]
package ifneeded drop_it_stats 1.0 [list source [file join $dir stats.tcl]]
package ifneeded drop_it_type 1.0 [list source [file join $dir type.tcl]]
