# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0

package provide drop_it_type 1.0

namespace eval type {

	set pname [opt::proc ::type --preset list --unique]
}