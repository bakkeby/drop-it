# Copyright (c) 2013, Stein Gunnar Bakkeby, all rights reserved.

package require tapp 1.0

package provide drop_it_flag 1.0

namespace eval flag {

	set pname [opt::proc ::flag --preset list --unique]
}

